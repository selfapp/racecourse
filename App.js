/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView
} from 'react-native';
import {Provider} from 'react-redux';
import { AppContainer } from './src/navigation/Router';
import configureStore from './src/store/configureStore';


const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{flex: 1}}>
          <AppContainer />
        </SafeAreaView>
      </Provider>
    );
  }
}

