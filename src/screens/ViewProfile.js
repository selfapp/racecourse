import React, {Component} from 'react';
import{
View,
Image,
ScrollView,
Dimensions,
TouchableOpacity,
Linking
} from 'react-native';
import { connect } from 'react-redux';
import {selectUserForBooking} from '../store/actions/booking';
import {calculateTimeDiff} from '../Utility/calculateTimeDiff';
import {
    BoldText, LightText
} from '../components/styledTexts';


const height = Dimensions.get('window').height;

class ViewProfile extends Component{
        constructor(props){
            super(props)
                this.state = {
                    userData:this.props.navigation.state.params.userData
                }
        }

        render(){
            return(
                <View style={{ flex: 1 }}>
 {/* Header  */}
                <View style={{ flex: .1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center'}}>
                            <TouchableOpacity style={{ height: 40, width: 40, alignItems:'center', justifyContent:'center' }}
                                onPress = {() => this.props.navigation.goBack()}
                            >
                                <Image
                                    source = { require('./../assets/back.png')}
                                    style={{ height: 25, width: 25 }}
                                    resizeMode='center'
                                />
                            </TouchableOpacity>
                            <View style={{ flexDirection:'row',flex:1, alignItems:'center',height: 40, justifyContent:'center'}}>
                                <BoldText>Profile Detail</BoldText>
                                <View style={{ width:40, height: 40}}/>
                            </View>
                        </View>
                    </View>

                <ScrollView style={{ flex: 1 }}>
                    <View style={{ flex:1 }}>
                        <Image
                        source = { this.state.userData.image ? {uri: this.state.userData.image} : require('./../assets/profiletemp.png')}
                        style = {{ flex: 1, height: height/3}}
                        resizeMode = 'cover'
                        />
                        <View style={{ bottom:5, paddingLeft: 10, width:'100%', position: 'absolute', justifyContent: 'flex-end', backgroundColor:'rgba(100,50,50,.1)' }}>
                        {(this.state.userData.userType === "Rider")?(
                            <BoldText style={{ color: 'white'}}>{this.state.userData.name}, {this.state.userData.age}</BoldText>
                        ):(
                            <BoldText style={{ color: 'white'}}>{this.state.userData.name}</BoldText>

                        )}
                            
                            <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                <Image
                                    source = { require('./../assets/mail.png')}
                                />
                                <BoldText style={{ color: 'white', fontSize: 12, paddingLeft: 5 }}>{this.state.userData.email},  {this.state.userData.userType}</BoldText>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, marginHorizontal: 10, marginVertical: 10 }}>
         {/* About Yourself */}
                        <View style={{ flexDirection:'row' }}>
                            <Image
                                source={ require('./../assets/about.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>About yourself:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.about}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
      {/* For Rider           */}
                {(this.state.userData.userType === "Rider")?(
                    <View>
{/* Height */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/cap.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Height:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.height}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Weight */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/pills.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Weight:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.weight}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Riding Hours */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/clock.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Riding Hours:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.hours}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Riding Style */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/history.png')}
                                style={{ height: 20, width: 20, tintColor:'black' }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Riding Style:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.style}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>

                    </View>
                ):(
    //  For Trainer               
                    <View>
{/* Race Track */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/cap.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Race Track:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.racetrack}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Training Hours */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/clock.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Training Hours:</BoldText>
                        </View>
                        {/* <LightText style={{ paddingLeft: 30 }}>{calculateTimeDiff(this.state.userData.startTime, this.state.userData.endTime)}</LightText> */}
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.startTime + '-' + this.state.userData.endTime}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Number of training */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/training-hourse.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Number of Training:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.hours}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Link of racetrack */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/track-link.png')}
                                style={{ height: 20, width: 20, tintColor:'black' }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Link of RaceTrack:</BoldText>
                        </View>
                        <TouchableOpacity
                        onPress={() => {
                            const words = this.state.userData.linkOfRaceTrack.split(':');
                            let racetrackLink = '';
                            if(words[0].toUpperCase() === 'HTTP' || words[0].toUpperCase() === 'HTTPS')
                            {
                                racetrackLink = this.state.userData.linkOfRaceTrack
                            }else{
                                racetrackLink = "http://" + this.state.userData.linkOfRaceTrack
                            }
                            Linking.canOpenURL(racetrackLink).then(supported => {
                                if (supported) {
                                  Linking.openURL(racetrackLink);
                                } else {
                                //   console.log("Don't know how to open URI: " + this.state.userData.linkOfRaceTrack);
                                }
                              });              
                        }}
                        >
                        <LightText style={{ paddingLeft: 30 }}>{this.state.userData.linkOfRaceTrack}</LightText>
                        </TouchableOpacity>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>

                    </View>
                )
                }
                    </View>

{/* Bottom Button             */}
                     <View style={{flex: 1, justifyContent: 'center',width: '80%', marginLeft: '10%'}}>
                                <TouchableOpacity
                                    style={{
                                        height: 40,
                                        backgroundColor: "rgb(196,31,20)",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 25
                                    }}
                                    onPress={() => 
                                        this.props.selectUserForBooking(this.state.userData,this.props.navigation)
                                    }
                                  >
                                    <BoldText style={{color: "#ffffff", fontSize: 15}}>Book</BoldText>
                                </TouchableOpacity>
                                <View style={{height: 50}}/>
                            </View>

                </ScrollView>
                </View>
            )
        }
};

const mapStateToProps = state =>{
    return{
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        selectUserForBooking:(user, navigation) => selectUserForBooking(user, dispatch, navigation),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ViewProfile);