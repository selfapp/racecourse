import React, {Component} from 'react';
import{
View,
Image,
TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import moment from "moment";
import {
    BoldText, LightText
} from '../components/styledTexts';

class AppointmentSuccess extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <View style={{ flex: 1 }}>
                    <View style={{ flex: .2, alignItems: 'center', justifyContent: 'center' }}>
                        <BoldText>Request Appointment</BoldText>
                    </View>
                    <View style={{ flex: 1, justifyContent:'center', alignItems:'center' }}>
                        <Image
                        source = { require('./../assets/congratulation.png')}
                        />
                    </View>
                    <View style={{ flex: 1, backgroundColor:'Blue'}}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <LightText style={{ fontSize: 20, textAlign: 'center', color:'#27294A'}}> {'Congratulation \n your appointment \n successfully Done'}</LightText>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <TouchableOpacity
                                    style={{
                                        height: 40,
                                        width: 100,
                                        backgroundColor: "rgb(196,31,20)",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 25
                                    }}
                                      onPress={() => this.props.navigation.popToTop()}
                                  >
                                    <BoldText style={{color: "#ffffff", fontSize: 15}}>OK</BoldText>
                                    </TouchableOpacity>
                            </View>

                    </View>
            </View>
        );
    }
}
const mapStateToProps = state =>{
    return{

    }
}
const mapDispatchToProps = dispatch =>{
    return {

    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AppointmentSuccess);