import React, {Component} from 'react';
import{
View,
Image,
Dimensions,
TouchableOpacity,
FlatList
} from 'react-native';
import { connect } from 'react-redux';
import moment from "moment";
import {
    BoldText, LightText
} from '../components/styledTexts';
import CalendarStrip from 'react-native-calendar-strip';
import { navigateToConfirm} from './../store/actions/user';
import { 
    selectDate, 
    fetchBooking, 
    selectSlot 
} from '../store/actions/booking';
import {OptionButton} from '../components/button';


const width = Dimensions.get('screen').width;

class Availability extends Component{
    constructor(props){
        super(props);
        this.state = {
            slots:[]
        }
    }

    componentDidMount(){
         this.props.selectDate(new Date(Date.now()));
         this.setState({slots: this.calculateSlots(this.props.bookingUser.startTime, this.props.bookingUser.endTime) }) 
         this.props.fetchBookingList( moment(this.props.selectedDate).format("dddd DD-MM-YYYY"), this.props.bookingUser.user)
    }
    selectedDate(date){
        this.props.selectDate(date);
        this.props.fetchBookingList( moment(date).format("dddd DD-MM-YYYY"), this.props.bookingUser.user)
    }

    isSlotBooked(id){
        const bookedSlot = this.props.bookingList.find(item => item.slot.id === id);
        if(bookedSlot){
            return true
        }else{
            return false
        }
    }


    calculateSlots(start, end){
        var startTime = moment(start, 'hh:mm A');
        var endTime = moment(end, 'hh:mm A');
        var index = 0;
        if( endTime.isBefore(startTime) ){
          endTime.add(1, 'day');
        }
        var timeStops = [];
        while(startTime < endTime){
            let start =  new moment(startTime)
            let end = new moment(startTime.add(60, 'minutes'))
            if(end > endTime){
                end = new moment(startTime.subtract(30, 'minutes'))
            }
            timeStops.push({
                    id:index + 1,
                    slot: start.format('hh:mm A') + ' - ' + end.format('hh:mm A')
                });
                index = index + 1;
        }
        return timeStops;
    }
    selectedItem(index){
        this.props.selectSlot(this.state.slots[index])
    }

    
    render(){
        return(
            <View style={{ flex: 1 }}>
                    <View style={{ flex: .1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center'}}>
                            <TouchableOpacity style={{ height: 40, width: 40, alignItems:'center', justifyContent:'center' }}
                                onPress = {() => this.props.navigation.goBack()}
                            >
                                <Image
                                    source = { require('./../assets/back.png')}
                                    style={{ height: 25, width: 25 }}
                                    resizeMode='center'
                                />
                            </TouchableOpacity>
                            <View style={{ flexDirection:'row',flex:1, alignItems:'center',height: 40, justifyContent:'center'}}>
                                <BoldText>Availability {this.props.user.userType === 'Rider' ? 'Trainer' : 'Rider' }</BoldText>
                                <View style={{ width:40, height: 40}}/>
                            </View>
                        </View>
                    </View>
{/* Calendar View */}
                    <View style={{ flex: .3, alignItems:'center', justifyContent:'center'}}>
                            <CalendarStrip 
                                daySelectionAnimation={{type: 'background', highlightColor: 'rgb(196,31,20)'}}
                                style={{flex: 1, width:width }}
                                calendarHeaderStyle={{color: 'black'}}
                                calendarColor={'white'}
                                dateNumberStyle={{color: 'black'}}
                                dateNameStyle={{color: 'black'}}
                                minDate = {new Date(Date.now())}
                                onDateSelected = { date => this.selectedDate(date)}
                            />
                            <LightText>{moment(this.props.selectedDate).format("dddd DD-MM-YYYY")}</LightText>
                            <LightText style={{ marginTop: 2, color: 'red' }}>{this.props.errors.select_slot ? this.props.errors.select_slot[0] : null}</LightText>
                    </View>
{/* Time Options */}
                    <View style={{ flex: 1,marginHorizontal: 20}}>
                        <View style={{ flex: 6, marginVertical: 20, justifyContent:'center'}}>

                        <FlatList
                                style={{ marginTop: 20, marginHorizontal:20 }}
                                data={this.state.slots}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item, index}) =>(
                                    <View style={{ padding: 10 }}>
                                        <OptionButton
                                        disabled={this.isSlotBooked(item.id)}
                                        value={item.slot}
                                        active = {(this.props.selectedSlot && (this.props.selectedSlot.id === item.id)) ? true : false }
                                        color={'rgb(196,31,20)'}
                                        onPress={ () => this.selectedItem(index)}
                                    />
                                </View>
                                )}
                                />
                        </View>
 {/* Bottom Button             */}
                        <View style={{flex: 1, justifyContent: 'center',width: '80%', marginLeft: '10%'}}>
                                <TouchableOpacity
                                    style={{
                                        height: 40,
                                        backgroundColor: "rgb(196,31,20)",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 25
                                    }}
                                    onPress={() => this.props.navigateToConfirm(this.props.selectedSlot, this.props.navigation)}
                                  >
                                    <BoldText style={{color: "#ffffff", fontSize: 15}}>Continue</BoldText>
                                </TouchableOpacity>
                            </View>
                    </View>
            </View>
        )
    }
}

const mapStateToProps = state =>{
    return{
        user: state.user.userData,
        bookingUser: state.booking.user,
        selectedDate: state.booking.selectedDate,
        selectedSlot : state.booking.selectedSlot,
        bookingList : state.booking.bookingList,
        errors: state.user.errors
    }
}
const mapDispatchToProps = dispatch => {
    return {
        selectSlot: (slot) => {
            dispatch(selectSlot(slot));
        },
        selectDate: (date) =>{
            dispatch(selectDate(date));
        },
        fetchBookingList:(date, userId) => fetchBooking(date, userId, dispatch),
        navigateToConfirm:(selectedSlot,navigation) => navigateToConfirm(selectedSlot, dispatch, navigation)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Availability);