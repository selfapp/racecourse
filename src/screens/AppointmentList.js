import React, {Component} from 'react';
import{
View,
Image,
FlatList,
TouchableOpacity,
Linking
} from 'react-native';
import { connect } from 'react-redux';
import moment from "moment";
import Loader from '../components/Loader';
import {fetchAppointmentList} from '../store/actions/booking';
import {
    BoldText, LightText
} from '../components/styledTexts';
import { checkDateDiff } from '../Utility/calculateTimeDiff';

class AppointmentList extends Component{
        constructor(props){
            super(props)
            this.state = {
            }
        }

    componentDidMount(){
        
          this.props.fetchAppointmentList( this.props.user.user, this.props.user.userType === "Rider" ? "Rider" : "Trainer");
    }

        renderItem = ({ item, index}) =>{
            return(
                <View style={{paddingHorizontal: 10, paddingVertical: 5}}
                    >
                    <View style={{ flex: 1,borderRadius: 10, backgroundColor: '#F7F7F7', shadowOffset:{width:0, height:5}, shadowOpacity: 0.18, shadowRadius:21, shadowColor: '#451B2D' }}>
                    <View style={{flex:3 , justifyContent:'center', flexDirection:'row'}}>
                            <Image
                            source = { (item.user.image != '') ? {uri: item.user.image} : require('./../assets/profile.png')}
                            style={{ height: 50, width: 50, margin: 10, borderRadius: 25 }}
                            />

                        <View style={{ flex: 1 , margin: 2, marginVertical:10, }}>
                            <BoldText style={{ fontSize: 14 }}>{item.user.name}</BoldText>
                            <LightText>{item.user.email}</LightText>
                        </View>
                    </View>
                <View style={{ marginHorizontal: 10}}>
                    <View style={{ flex: 1}}>
                        <View style={{ flex: .6, flexDirection:'row'}}>
                            <BoldText style={{ fontSize:12, color:'rgba(0,0,0,.7)'}}>Date:</BoldText>
                            <LightText>{item.data.date}</LightText>
                        </View>
                        <View style={{ flex: .4, flexDirection:'row', alignItems:'center'}}>
                            <BoldText style={{ fontSize:12, color:'rgba(0,0,0,.7)'}}>Time:</BoldText>
                            <LightText style={{flex:1, marginHorizontal:1}}>{item.data.slot.slot}</LightText>
                        </View>
                    </View>
                    {item.user.userType === 'Rider' ? (
                        <View style={{height: 5}}/>
                    ) :(
                        <View style={{ flex: .6, flexDirection:'row', marginVertical: 5}}>
                        <BoldText style={{ fontSize:12, color:'rgba(0,0,0,.7)'}}>Link of RaceTrack:</BoldText>
                        <TouchableOpacity
                            onPress={() => {
                                const words = item.user.linkOfRaceTrack.split(':');
                                let racetrackLink = '';
                                if(words[0].toUpperCase() === 'HTTP' || words[0].toUpperCase() === 'HTTPS')
                                {
                                    racetrackLink = item.user.linkOfRaceTrack
                                }else{
                                    racetrackLink = "http://" + item.user.linkOfRaceTrack
                                }
                                Linking.canOpenURL(racetrackLink).then(supported => {
                                    if (supported) {
                                    Linking.openURL(racetrackLink);
                                    } else {
                                    // console.log("Don't know how to open URI: " + item.user.linkOfRaceTrack);
                                    }
                                });              
                            }}
                            >
                        <LightText>{item.user.linkOfRaceTrack}</LightText>
                        </TouchableOpacity>
                </View>
                    )}
                    
                </View>
                </View>
                </View>
            );
        }

        empetyList =() =>{
            return (
            <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
            <BoldText style={{ color: "grey", justifyContent: "center", fontSize: 20 }}>
                {" "}
                No Appointments.
            </BoldText>
            </View>
            );
        }
        renderSeparater = () => {
            return(
                <View style={{ flex: 1, height: 0, backgroundColor:'#fff' }}></View>
            )
        }
        
        render(){

            return(
                <View style={{ flex: 1 }}>
                    <View style={{ flex: .1, alignItems: 'center', justifyContent: 'center' }}>
                        <BoldText>Appointment List</BoldText>
                    </View>
                    <View style={{ flex: 1, backgroundColor:'#F7F7F7'}}>
                   
                    <FlatList
                         removeClippedSubviews={false}
                         data = {this.props.appointmentList}
                         keyExtractor = {(item, index) => index.toString()}
                         style={{ flex: 1 }}
                         renderItem = {this.renderItem}
                         ItemSeparatorComponent = {this.renderSeparater}
                         ListEmptyComponent = { this.empetyList}
                        />
                </View>
                <Loader loading={this.props.loading} />
                </View>
            )
        }
};

const mapStateToProps = state =>{
    return{
        user: state.user.userData,
        userList: state.users.userList,
        appointmentList: state.booking.appointmentList,
        loading: state.booking.loading
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        fetchAppointmentList:(userId, userType) => fetchAppointmentList(userId, userType, dispatch)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AppointmentList);