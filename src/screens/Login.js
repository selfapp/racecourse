import React, {Component} from 'react';
import{
    View,
    KeyboardAvoidingView,
    Platform,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import CountryPicker from 'react-native-country-picker-modal';
import Loader from '../components/Loader';
import {
    BoldText,
    LightText
} from '../components/styledTexts';
import { typeMobile, chooseCountry, login } from '../store/actions/user';

let height1 = Dimensions.get("window").height;
const NORTH_AMERICA = ["AF", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "VG", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CK", "CR", "HR", "CU", "CW", "CY", "CZ", "CD", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "CI", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "XK", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "KP", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "CG", "RO", "RU", "RW", "RE", "BL", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "SA", "SN", "RS", "SC", "SL", "SG", "SX", "SK", "SI", "SB", "SO", "ZA", "GS", "KR", "SS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "ST", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "VI", "UY", "UZ", "VU", "VA", "VE", "VN", "WF", "EH", "YE", "ZM", "ZW", "AX"]

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            cca2: 'US',
        }
    }


    render(){
        return(
            <View style = {{ flex: 1}}>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>

                <KeyboardAvoidingView
                 behavior={Platform.OS === "ios" ? "padding" : null}
                 style={{flex: 1}}
                 keyboardVerticalOffset={Platform.select({ios: -10, android: 20})}
                >
                    <View style={{ flex: 0.3 }}/>
                    <View style={{flex:1, alignItems:'center', justifyContent:'space-evenly' }}>
                        <View style={{ flexDirection:'row' }}>
                            <BoldText style={{ fontSize: 45, color: 'rgb(196,31,20)' }}>Back</BoldText>
                            <BoldText style={{ fontSize: 45, color:'rgb(119,120,122)' }}>Side</BoldText>
                        </View>
                        <LightText style={{ textAlign:'center'}}>{'BackSide is an app for on demand access to exercise riders availability during morning training of racehorses and an organizer for riders to view and share their availability.'}</LightText>
                    </View>

                    
                    <View style={{flex: 1, marginHorizontal: 20}}>
                            <View style={[{ borderWidth: 1, borderColor: 'transparent'}, this.props.errors.phone_number ? styles.errorInput : {}]}>
                                <View style={[{flexDirection: 'row', alignItems: 'center'}]}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <CountryPicker
                                            countryList={NORTH_AMERICA}
                                            filterable={true}
                                            filterPlaceholder={'Search'}
                                            transparent={true}
                                            onChange={value => {
                                                this.setState({cca2: value.cca2});
                                                this.props.chooseCountry(value.callingCode);
                                            }}
                                            cca2={this.state.cca2}
                                            callingCode={this.props.country_code}
                                            showCallingCode={true}
                                        />
                                        <BoldText
                                            style={{color: 'rgb(119,120,122)', fontSize: 20}}> +{this.props.country_code}</BoldText>
                                    </View>
                                    <View style={{
                                        width: 1,
                                        height: 30,
                                        marginLeft: 10,
                                        backgroundColor: 'rgba(119,120,122,0.9)'
                                    }}/>

                                    <TextInput
                                        maxLength={10}
                                        underlineColorAndroid='transparent'
                                        autoCorrect={false}
                                        returnKeyType='done'
                                        keyboardType='numeric'
                                        placeholder="Enter your phone"
                                        placeholderTextColor='rgb(119,120,122)'
                                        fontSize={18}
                                        onChangeText={(phone_number) => this.props.phoneNumberChange(phone_number)}
                                        style={[styles.normalInput]}
                                    />
                                </View>
                                <View style={{height: 1, backgroundColor: 'rgb(119,120,122)'}}/>
                            </View>
                            <LightText style={{ marginTop: 2, color: 'red' }}>{this.props.errors.phone_number ? this.props.errors.phone_number[0] : null}</LightText>

                            <View style={{flex: 1, justifyContent: 'center', marginHorizontal:20}}>
                                <TouchableOpacity
                                    style={{
                                        height: 40,
                                        backgroundColor: "rgb(196,31,20)",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 25
                                    }}
                                    onPress={() => {
                                         this.props.login(this.props.country_code, this.props.phone_number, this.props.navigation.navigate)}
                                    }
                                >
                                    <BoldText style={{color: "#ffffff", fontSize: height1 > 600 ? 20 : 15}}>Login</BoldText>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1}}/>
                        </View>
                        <Loader loading={this.props.loading} />


                </KeyboardAvoidingView>
                </TouchableWithoutFeedback>
            </View>
        )
    }

};
const styles = StyleSheet.create({
    normalInput: {
        height: 40,
        color: 'rgb(119,120,122)',
        marginLeft: 10,
        flex: 1
    },
    errorInput: {
        borderColor: 'red'
    }
});

const mapStateToProps = state =>{
    return{
        phone_number: state.user.phone_number,
        country_code: state.user.country_code,
        loading: state.user.loading,
        errors: state.user.errors
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        phoneNumberChange: (phone_number) => {
            dispatch(typeMobile(phone_number));
        },
        chooseCountry: (country_code) => {
            dispatch(chooseCountry(country_code));
        },
        login:(country_code, phone_number,navigate) => login(country_code, phone_number,dispatch, navigate)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);