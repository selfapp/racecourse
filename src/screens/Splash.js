import React, {Component} from 'react';
import {View, Text} from 'react-native';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux';
import {
    updateUid,
    updateUserData
} from '../store/actions'

class Splash extends Component{
    constructor(props){
        super(props);
        this.state = {
        }
    }
    componentWillMount(){
        var _this = this;
        firebase.auth().onAuthStateChanged( async (user) => {
            if (user) {
                this.props.updateUid(user.uid);
                await firebase.database().ref('Users/' + user.uid).on('value', function (snapshot){
                    let data = snapshot.val();
                    if (data != null) {
                        _this.props.updateUserData(data);
                        if(data.userType !== ''){
                            if(data.isProfileCompleted){
                                
                            _this.props.navigation.navigate('TabNavigator');
                            }else{
                                _this.props.navigation.navigate('EditProfile', {fromUserType:true});
                            }
                        }else{
                            _this.props.navigation.navigate('IntroduceYourself');
                        }
                    }else{
                        firebase.database().ref('Users/' + user.uid).set({
                            user:user.uid,
                            isProfileCompleted: false,
                            name:'',
                            email:'',
                            image:'',
                            about:'',
                            height:'',
                            weight:'',
                            userType:'',
                            age:'',
                            hours:'',
                            style:''
                        }).then(()=>{
                            _this.props.navigation.navigate('IntroduceYourself');
                        })
                    }
             } )
            } else {
                this.props.navigation.navigate('loginNavigationOptions')
            }
        });
    }

    render(){
        return(
            <View style={{ flex: 1, alignItems:'center'}}>
            </View>
        )
    }
}

const mapStateToProps = state =>{
    return {
        uid: state.user.uid
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateUid: (uid) => updateUid(uid, dispatch),
        updateUserData:(data) => updateUserData(data, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);