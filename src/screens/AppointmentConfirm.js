import React, {Component} from 'react';
import{
View,
Image,
TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { navigateToSuccess} from './../store/actions/user';
import { bookSlot } from '../store/actions/booking';
import Loader from '../components/Loader';
import moment from "moment";
import {
    BoldText, LightText
} from '../components/styledTexts';


class AppointmentConfirm extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <View style={{ flex: 1 }}>
                <View style={{ flex: .1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center'}}>
                            <TouchableOpacity style={{ height: 40, width: 40, alignItems:'center', justifyContent:'center' }}
                                onPress = {() => this.props.navigation.goBack()}
                            >
                                <Image
                                    source = { require('./../assets/back.png')}
                                      style={{ height: 25, width: 25 }}
                                      resizeMode='center'
                                />
                            </TouchableOpacity>
                            <View style={{ flexDirection:'row',flex:1, alignItems:'center',height: 40, justifyContent:'center'}}>
                                <BoldText>Confirm appointment</BoldText>
                                <View style={{ width:40, height: 40}}/>
                            </View>
                        </View>
                    </View>

                    <View style={{ flex:.1, alignItems:'center', justifyContent:'center'}}>
                                <BoldText style={{ textAlign:'center',color:'rgba(0,0,0,.8)' }}>{'You should able to \n Appointment'}</BoldText>
                    </View>

{/* User Details */}
                    <View style={{ flex: 1, marginVertical: 20}}>
                        <View style={{ flex: 1, flexDirection: 'row',  backgroundColor: '#F7F7F7' }}>
                            <View style={{flex:3 , alignItems:'center', justifyContent:'center'}}>
                                 <View style={{ flexDirection:'row'}}>
                                     <Image
                                         source = { (this.props.bookingUser.image != '') ? {uri: this.props.bookingUser.image} : require('./../assets/profile.png')}
                                        style={{ height: 50, width: 50, margin: 10, borderRadius: 25 }}
                                    />

                                <View style={{ flex: 1 , margin: 2, justifyContent:'space-evenly'}}>
                                    <BoldText style={{ fontSize: 14 }}>{this.props.bookingUser.name}</BoldText>
                                    <LightText>{this.props.bookingUser.userType}</LightText>
                                 </View>
                                </View>
                            </View>
                        </View>
{/* Time Details  */}
                        <View style={{ flex: 2, flexDirection: "row", backgroundColor: '#F7F7F7', padding:15}}>
                            <View style={{ flex: 1 }}>
                                <BoldText style={{ fontSize: 12}}>DATE</BoldText>
                                <View style={{ flex: 1, marginTop: 15}}>
                                    <BoldText>{moment(this.props.selectedDate).format('DD MMM')}</BoldText>
                                    <LightText>{moment(this.props.selectedDate).format('dddd')}</LightText>
                                    <View style={{marginTop: 10, width: '80%' , height: 1, backgroundColor: 'rgb(196,31,20)' }}/>
                                </View>
                            </View>
                            <View style={{ flex: 1 }}>
                                <BoldText style={{ fontSize: 12}}>TIME</BoldText>
                                <View style={{ flex: 1, marginTop: 15}}>
                                    <BoldText>{this.props.slot.slot}</BoldText>
                                    <LightText></LightText>
                                    {/* <LightText>PM</LightText> */}
                                    <View style={{marginTop: 10, width: '80%' , height: 1, backgroundColor: 'rgb(196,31,20)' }}/>
                                </View>
                            </View>
                        </View>

   {/* Bottom Button    */}
                     <View style = {{ flex: 1, backgroundColor: '#F7F7F7', alignItems: 'center'}}>
                        <View style={{flex: 1, justifyContent: 'center', width: '80%'}}>
                                <TouchableOpacity
                                    style={{
                                        height: 40,
                                        backgroundColor: "rgb(196,31,20)",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 25
                                    }}
                                     onPress = {() => this.props.bookSlot(this.props.bookingUser.user, this.props.currentUser.user, moment(this.props.selectedDate).format("dddd DD-MM-YYYY"), this.props.slot, this.props.navigation)}
                                  >
                                    <BoldText style={{color: "#ffffff", fontSize: 15}}>Confirm Appointment</BoldText>
                                </TouchableOpacity>
                            </View>
                            </View>
                            <View style={{ flex: 3 }}/>
                    </View>
                    <Loader loading={this.props.loading} />
            </View>
        )
    }
}

const mapStateToProps = state =>{
    return{
        bookingUser: state.booking.user,
        currentUser:state.user.userData,
        selectedDate: state.booking.selectedDate,
        slot: state.booking.selectedSlot,
        loading: state.booking.loader
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        bookSlot:(trainerID, riderID, date, slot, navigation) => bookSlot(trainerID, riderID, date, slot, dispatch,navigation),
        navigateToSuccess:(navigation) => navigateToSuccess(navigation),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AppointmentConfirm);