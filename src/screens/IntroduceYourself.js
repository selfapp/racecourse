import React, {Component} from 'react';
import{
View,
Image,
TouchableOpacity,
Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import {
    BoldText, LightText
} from '../components/styledTexts';
import { setUserType } from './../store/actions/user';
import Loader from '../components/Loader';

const height = Dimensions.get('window').height;

class IntroduceYourself extends Component{
    constructor(props){
        super(props)

    }

    render(){
        return(
            <View style={{flex:1, backgroundColor:'rgb(255,255,255)'}}>
                <View style={{ flex: 1, alignItems:'center', justifyContent:'center' }}>
                    <BoldText>Are you a</BoldText>
                </View>
{/* TRAINER */}
                <View style={{ flex: 1, alignItems:'center' }}>
                    <View style={{ flex: 1}}/>
                    <TouchableOpacity style={{ borderRadius: 5, flex: 1.5, width: '80%',alignItems:'center',justifyContent:'center', backgroundColor:'rgb(245,245,245)' }}
                    onPress={() => this.props.setUserType('Trainer', this.props.uid,this.props.navigation)}
                    >
                        <BoldText style={{color: 'rgb(119,120,122)'}}>TRAINER</BoldText>
                    </TouchableOpacity>
                    <View style={{height: height/ 8, width: height/8, borderRadius: (height/8)/2, alignItems:'center', justifyContent:'center', position:'absolute', backgroundColor: 'rgb(196,31,20)'}}>
                        <Image
                            source = {require('./../assets/trainer.png')}
                            // style = {{height: 60, width:60}}
                            resizeMode = 'contain'
                        />
                    </View>
                    <View style={{ flex: .1}}/>

                </View>
 {/* RIDER */}
                <View style={{ flex: 1, alignItems:'center' }}>
                <View style={{ flex: 1}}/>
                    <TouchableOpacity style={{ borderRadius: 5, flex: 1.5, width: '80%',alignItems:'center',justifyContent:'center', backgroundColor:'rgb(245,245,245)' }}
                    onPress={() => {
                         this.props.setUserType('Rider', this.props.uid,this.props.navigation)}
                        }
                    >
                        <BoldText style={{color: 'rgb(119,120,122)'}}>RIDER</BoldText>
                    </TouchableOpacity>
                    <View style={{height: (height/8), width: (height/8), borderRadius: (height/8)/2, alignItems:'center', justifyContent:'center', position:'absolute', backgroundColor: 'rgb(196,31,20)'}}>
                        <Image
                            source={require('./../assets/rider.png')}
                            // style={{height: 60, width:60}}
                            resizeMode = 'contain'
                        />
                    </View>
                    <View style={{ flex: .1}}/>
                </View>
                <View style={{ flex: 1 }}></View>
                <Loader loading={this.props.loading} />
            </View>
        );
    }  
};

const mapStateToProps = state =>{
    return{
        uid:state.user.uid,
        loading: state.user.loading,
    }
}
const mapDispatchToProps = dispatch =>{
    return{
        setUserType:(userType,uid,navigation) => setUserType(userType, uid,dispatch,navigation)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(IntroduceYourself);