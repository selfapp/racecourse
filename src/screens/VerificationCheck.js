import React, {Component} from 'react';
import{
View,
TouchableOpacity,
KeyboardAvoidingView,
Image,
Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import CodeInput from 'react-native-confirmation-code-input';
import Loader from '../components/Loader';
import {verifyOTP, reSendOtp} from "../store/actions/user";
import {
    BoldText,
    LightText
} from '../components/styledTexts';

let {height, width} = Dimensions.get('window');

class VerificationCheck extends Component {
    constructor(props){
        super(props);
        this.state = {
            loader:false,
            otp:''
        }

    }

render(){
    return(
        <View>
           
           <KeyboardAvoidingView contentContainerStyle={{flex: 1}} 
                        style={{height: height, width: width}}
                        behavior='position' enabled 
                         keyboardVerticalOffset={-250}
                         >
                        <View style={{flex: 1, marginHorizontal: 30, paddingVertical: 10}}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableOpacity
                                    style={{flex: 0.5}}
                                    onPress={() => {
                                        this.props.navigation.goBack()
                                    }}
                                >
                                    <Image style={{height: 30, width: 30,}}
                                           resizeMode='contain'
                                        //    source={require('./../assets/back-button.png')}
                                    />
                                </TouchableOpacity>
                                <BoldText style={{color: 'rgb(119,120,122)', fontSize: 20}}>Verification</BoldText>
                                <View style={{flex: 0.5}}/>
                            </View>
                            <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
                                <View style={{ flexDirection:'row' }}>
                                    <BoldText style={{ fontSize: 45, color: 'rgb(196,31,20)' }}>Back</BoldText>
                                    <BoldText style={{ fontSize: 45, color:'rgb(119,120,122)' }}>Side
                                    </BoldText>
                                 </View>
                            </View>
                            <BoldText style={{textAlign: 'center', color: 'rgb(119,120,122)'}}>We have sent you an
                                access code on
                                +{this.props.navigation.state.params.country_code} {this.props.navigation.state.params.phoneNumber} via
                                SMS for Mobile number verification</BoldText>
                            <View style={{flex: 0.7, alignItems: 'center', justifyContent: 'center'}}>
                                <LightText style={{ color: 'red', textAlign: 'center', marginTop: 10 }}>{this.props.errors.otp ? this.props.errors.otp[0] : null}</LightText>
                                
                                <CodeInput
                                    ref="codeInputRef1"
                                    secureTextEntry
                                    returnKeyType='done'
                                    keyboardType='numeric'
                                    codeLength={6}
                                    activeColor='rgba(196,31,20, 1)'
                                    inactiveColor='rgba(119, 120, 122, 1)'
                                    className={'border-b'}
                                    space={5}
                                    size={40}
                                    inputPosition='left'
                                    onFulfill={(code) => {
                                        this.setState({ otp: code });
                                    }}
                                />
                            </View>

                            <TouchableOpacity
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    height: 45,
                                    borderRadius: 22.5,
                                    backgroundColor: "rgb(196,31,20)",
                                    marginHorizontal:30 ,
                                    marginTop: 40 
                                    }}
                                onPress={() => this.props.verifyOTP(this.state.otp, this.props.confirmResult, this.props.navigation)}
                            >
                                <BoldText style={{color: 'white'}}>Continue</BoldText>
                            </TouchableOpacity>

                            <View style={{flex: 1, justifyContent: 'center'}}>
                                <View style={{height: 1, backgroundColor: 'rgb(119,120,122)'}}/>
                                <View style={{flexDirection: 'row', marginTop: 5}}>
                                    <View style={{flex: 0.5}}/>
                                    <TouchableOpacity style={{}}
                                    onPress={()=> this.props.reSendOtp(this.props.navigation.state.params.country_code, this.props.navigation.state.params.phoneNumber)}
                                    >
                                        <BoldText style={{
                                            color: 'rgb(119,120,122)',
                                            letterSpacing: 1,
                                            textAlign: 'center'
                                        }}>RESEND CODE</BoldText>
                                    </TouchableOpacity>
                                    <View style={{flex: 0.5}}/>
                                </View>
                            </View>
                            <View style={{flex: 0.5}}/>
                        </View>
                        <Loader loading={this.props.loading} />
                    </KeyboardAvoidingView>
        </View>
    )
}
};
const mapStateToProps = state =>{
    return {
        loading: state.user.loading,
        errors: state.user.errors,
        message: state.user.message,
        confirmResult: state.user.confirmResult
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        verifyOTP:(code,confirmResult,navigation) => verifyOTP(code,confirmResult,dispatch,navigation),
        reSendOtp:(country_code, phoneNumber) => reSendOtp(country_code, phoneNumber, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VerificationCheck);