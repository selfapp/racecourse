import React, {Component} from 'react';
import{
View,
Image,
FlatList,
TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import {selectUserForBooking} from '../store/actions/booking';
import {
    BoldText, LightText
} from '../components/styledTexts';
import {
    fetchUsers
} from '../store/actions'


class Book extends Component{
        constructor(props){
            super(props)

        }

    componentDidMount(){
        this.props.fetchUsers(this.props.user.userType === "Rider" ? "Trainer" : "Rider");
    }


        renderItem = ({ item}) =>{
            return(
                <View style={{paddingHorizontal: 10, paddingVertical: 5}}>
                <TouchableOpacity style={{ flex: 1, flexDirection: 'row', borderRadius: 10, backgroundColor: '#F7F7F7',shadowOffset:{width:0, height:5}, shadowOpacity: 0.18, shadowRadius:21, shadowColor: '#451B2D'  }}
                    onPress={()=>this.props.navigation.navigate('ViewProfile',{userData:item})}
                    >
                    <View style={{flex:3 , alignItems:'center', justifyContent:'center'}}>
                        <View style={{ flexDirection:'row'}}>
                            <Image
                            source = { (item.image != '') ? {uri: item.image} : require('./../assets/profile.png')}
                            style={{ height: 50, width: 50, margin: 10, borderRadius: 25 }}
                            />

                        <View style={{ flex: 1 , margin: 2, justifyContent:'space-evenly'}}>
                            <BoldText style={{ fontSize: 14 }}>{item.name}</BoldText>
                            <LightText>{item.userType}</LightText>
                        </View>
                        </View>
                    </View>
                    {this.props.user.userType === "Rider" ?(
                    <View style={{ flex:1, alignItems:'center', justifyContent: 'center'}}>
                        <TouchableOpacity style={{backgroundColor:'#27294A', paddingHorizontal: 10, borderRadius:15}}
                                         onPress={() => this.props.selectUserForBooking(item,this.props.navigation)}
                        >
                            <BoldText style={{ color: 'white', fontSize:12, margin: 5 }}>Book</BoldText>
                        </TouchableOpacity>
                    </View>
                    ):(null)}
                    
                </TouchableOpacity>
                </View>
            );
        }

        empetyList =() =>{
            return (
            <View style={{ alignItems: "center", justifyContent: "center", marginTop: 100 }}>
            {/* <BoldText style={{ color: "red", justifyContent: "center", fontSize: 20 }}>
                {" "}
                No Items Found
            </BoldText> */}
            </View>
            );
        }
        renderSeparater = () => {
            return(
                <View style={{ flex: 1, height: 0 }}></View>
            )
        }

        render(){
            return(
                <View style={{ flex: 1 }}>
                    <View style={{ flex: .1, alignItems: 'center', justifyContent: 'center' }}>
                        <BoldText>Availability {this.props.user.userType === 'Rider' ? 'Trainers' : 'Riders' }</BoldText>
                    </View>
                    <View style={{ flex: 1, backgroundColor:'#F7F7F7'}}>
                        <FlatList
                         data = {this.props.userList}
                         keyExtractor = {(item, index) => index.toString()}
                         style={{ flex: 1 }}
                         renderItem = {this.renderItem}
                         ItemSeparatorComponent = {this.renderSeparater}
                         ListEmptyComponent = { this.empetyList}
                        />
                    </View>
                </View>
            )
        }
};

const mapStateToProps = state =>{
    return{
        user: state.user.userData,
        userList: state.users.userList
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        selectUserForBooking:(user, navigation) => selectUserForBooking(user, dispatch, navigation),
        fetchUsers:(userType) => fetchUsers(userType, dispatch)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Book);