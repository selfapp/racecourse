import React, {Component} from 'react';
import{
    View,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Platform,
    Dimensions,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import DatePicker from 'react-native-datepicker'
import { BoldText, LightText} from '../components/styledTexts';
import Slider from 'react-native-slider';
import {
    updateRiderProfile,
    updateTrainerProfile
} from '../store/actions/user';
import Loader from '../components/Loader';
import ActionSheet from 'react-native-actionsheet'

const {height, width} = Dimensions.get('window');

class EditProfile extends Component{
    constructor(props){
        super(props);
        this.state = {
            fromUserType:null,
            name:'',
            email:'',
            age:'',
            Weight:'',
            height:'',
            about:'',
            hours:5,
            style:'',
            ProfilePic:'',
            startTime:'00 AM',
            endTime:'00 AM',
            linkOfRaceTrack:''
        }
    }

    componentDidMount(){

        this.setState({
            name: this.props.user.name,
            email: this.props.user.email,
            about: this.props.user.about,
            ProfilePic: this.props.user.image,
            fromUserType: this.props.navigation.state.params.fromUserType
        })
        if(this.props.user.userType === 'Rider'){
            this.setState({
                age: this.props.user.age,
                weight: this.props.user.weight,
                height: this.props.user.height,
                hours: this.props.user.hours,
                style: this.props.user.style,    
            })
        }else{
            this.setState({
                startTime:this.props.user.startTime,
                endTime:this.props.user.endTime,
                linkOfRaceTrack: this.props.user.linkOfRaceTrack
            })
        }

    }


validateUserDetails(){
     const {name,email,age,weight,height,about,hours,style, ProfilePic, endTime, startTime, linkOfRaceTrack} = this.state;
     let reg = /\S+@\S+\.\S+/g ;

    if(name.length < 2){
        alert('Please enter name')
        return 
    }else if(reg.test(email) === false){
        alert('Please enter a valid email address.')
        return
    }
    if(this.props.user.userType === 'Rider'){
        this.props.updateRiderProfile(this.props.uid, name,email,age,about,hours,height,weight,ProfilePic, this.props.user.userType,style, this.state.fromUserType, this.props.navigation);
    }else{
        this.props.updateTrainerProfile(this.props.uid, name,email,about,ProfilePic,startTime,endTime,linkOfRaceTrack, this.props.user.userType, this.state.fromUserType, this.props.navigation);
    }
}

openGalleryForProfilePic(buttonIndex) {
    
            if(buttonIndex === 1) {
                ImagePicker.openPicker({
                    width: 1000,
                    height: 1000,
                    cropping: true,
                    includeBase64: true,
                    mediaType: 'photo'
                })
                    .then(image => {
                        this.setState({
                            ProfilePic: `data:${image.mime};base64,${image.data}`,
                        });
                    });
            } else if(buttonIndex === 0) {
                ImagePicker.openCamera({
                    width: 1000,
                    height: 1000,
                    cropping: true,
                    includeBase64: true,
                    mediaType: 'photo'
                })
                    .then(image => {
                        this.setState({
                            ProfilePic: `data:${image.mime};base64,${image.data}`,
                        });
                    });
            }
}

showActionSheet = () => {
    this.ActionSheet.show()
  }

    render(){
        return(
            <View style={{ flex: 1 }}>
                <View style={{ flex: .1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent:'center'}}>
                            <TouchableOpacity style={{ height: 40, width: 40, alignItems:'center', justifyContent:'center' }}
                                onPress = {() => this.props.navigation.goBack()}
                            >
                                <Image
                                    source = { require('./../assets/back.png')}
                                      style={{ height: 25, width: 25 }}
                                      resizeMode='center'
                                />
                            </TouchableOpacity>
                            <View style={{ flexDirection:'row',flex:1, alignItems:'center',height: 40, justifyContent:'center'}}>
                                <BoldText>Profile</BoldText>
                                <View style={{ width:40, height: 40}}/>
                            </View>
                        </View>
                    </View>
            
             <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "height" : null}
                    style={{flex: 1}}
                     keyboardVerticalOffset={Platform.select({ios: -20, android: 20})}
                    >
                    <ScrollView style={{ flex: 1}}>
                    <View style={{ flex: 1 ,marginHorizontal: 10}}>
{/* Profile Pic */}
                    <View style={{ justifyContent:'center', alignItems:'center', marginVertical: 10 }}>
                            <View style={{ height: 120, width: 120, borderRadius: 60, borderWidth: 2, borderColor: 'rgb(196,31,20)', justifyContent:'center', alignItems:'center' }}>
                                <View style={{ alignItems:'center', justifyContent:'center'}}>
                                <Image
                                    style={{ height: 110, width: 110, borderRadius: 55 }}
                                    source={(this.state.ProfilePic) ? {uri: this.state.ProfilePic} : require("../assets/profile.png")}
                                />
                                </View>
                            <TouchableWithoutFeedback 
                                 onPress={() => {
                                     this.showActionSheet();
                                }}
                                >
                                <View style={{ position: "absolute", width: 120,height: 120, justifyContent: 'center', alignItems: 'center',}}>
                                    <Image 
                                    source={require('./../assets/camera.png')}
                                    style={{ tintColor:'rgb(240,241,245)', height: 30, width: 30}}
                                    />
                                 </View>
                            </TouchableWithoutFeedback>
                            </View>           
                        </View>
{/* Actionsheet For Option */}
                        <ActionSheet
                            ref={o => this.ActionSheet = o}
                            title={'Which one do you like ?'}
                            options={['Take photo', 'Choose from Library', 'Cancel']}
                            cancelButtonIndex={2}
                            onPress={(index) => { this.openGalleryForProfilePic(index) }}
                            />
                        
                        <View>
                            <LightText style={{ color: 'rgb(196,31,20)', fontSize:16}}>BASIC INFO</LightText>
                        </View>

                           <View style={{ flex: 1, marginVertical: 5, borderRadius: 10, backgroundColor:'rgb(248,249,250)'}}>
                            <View style={{ padding: 10}}>
                                <LightText>Name</LightText>
                                <TextInput
                                        style={{ height: 45,fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                        placeholder ="Full name"
                                        keyboardType={'default'}
                                        value={this.state.name}
                                        onChangeText={(name) => this.setState({name:name})}
                                    />
                                </View>
                                <View style={{ height: 1, backgroundColor: 'rgba(139,139,139,.5)'}}/>

                                <View style={{ padding: 10}}>
                                <LightText>Email</LightText>
                                <TextInput
                                        style={{ height: 45,fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                        placeholder ="Email id"
                                        keyboardType={'email-address'}
                                        value={this.state.email}
                                        onChangeText={(email) => this.setState({email:email})}
                                    />
                                </View>
                        {this.props.user.userType === 'Rider' ? (
                              <View>
                                <View style={{ height: 1, backgroundColor: 'rgba(139,139,139,.5)'}}/>
                                <View style={{ padding: 10}}>
                                <LightText>Age</LightText>
                                <TextInput
                                        style={{ height: 45,fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                        placeholder ="Age"
                                        keyboardType={'decimal-pad'}
                                        value={this.state.age}
                                        onChangeText={(age) => this.setState({age:age})}
                                    />
                                </View>
                                <View style={{ height: 1, backgroundColor: 'rgba(139,139,139,.5)'}}/>
                                <View style={{ padding: 10}}>
                                <LightText>Weight</LightText>
                                <TextInput
                                        style={{ height: 45,fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                        placeholder ="Weight"
                                        keyboardType={'decimal-pad'}
                                        value={this.state.weight}
                                        onChangeText={(weight) => this.setState({weight: weight})}
                                    />
                                </View>
                                <View style={{ height: 1, backgroundColor: 'rgba(139,139,139,.5)'}}/>

                                <View style={{ padding: 10}}>
                                <LightText>Height</LightText>
                                <TextInput
                                        style={{ height: 45,fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                        placeholder ="Height"
                                        keyboardType={'decimal-pad'}
                                        value={this.state.height}
                                        onChangeText={(height) => this.setState({height:height})}
                                    />
                                </View>
                                </View>
                                
                                ) : (null)
                                }
                                
                                <View style={{marginBottom: 10, height: 1, backgroundColor: 'rgba(139,139,139,.5)'}}/>
                           </View>

                           <View style={{ marginVertical: 5}}>
                            <LightText style={{ color: 'rgb(196,31,20)', fontSize:16}}>ABOUT ME</LightText>
                            </View>

                            <View style={{ flex: 1, marginVertical: 5, borderRadius: 10, backgroundColor:'rgb(248,249,250)'}}>
                            <View style={{ margin: 10}}>
                                 <TextInput
                                    style={{ marginTop: 10, height: 100, textAlignVertical: 'top', fontSize:16, alignItems:"flex-start", color:'black', borderRadius: 5}} 
                                    placeholder ="About yourself."
                                    returnKeyType='next'
                                    autoCorrect={false}
                                    multiline={true}
                                    keyboardType='default'
                                    value={this.state.about}
                                    onChangeText={(about) => this.setState({about:about})}
                                />
                                </View>
                            </View>
{/* Riding details */}
                            <View style={{ marginVertical: 5}}>
                            <LightText style={{ color: 'rgb(196,31,20)', fontSize:16}}>{this.props.user.userType === 'Rider' ? "RIDING" : "TRANING" } DETAILS</LightText>
                            </View>
                            <View style={{ flex: 1, marginVertical: 5, borderRadius: 10, backgroundColor:'rgb(248,249,250)'}}>
{/* Time picker for Trainer                */}
                            {this.props.user.userType === 'Rider' ? (
                                <View style={{ margin: 10}}>
                                <LightText>Hours</LightText>

                            <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                            <LightText>0</LightText>
                            <LightText>{this.state.hours}</LightText>

                            </View>
                            <Slider
                                minimumValue={0}
                                maximumValue={10}
                                step={1}
                                minimumTrackTintColor={'rgb(196,31,20)'}
                                thumbTintColor={'rgb(196,31,20)'}
                                value={this.state.hours}
                                onValueChange={(val) => this.setState({ hours: val})} 
                                />
                            </View>
                            ) : (
                            <View style={{ margin: 10}}>
                                <View style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                                    <LightText>Start Time</LightText>                                  
                                    <LightText>End Time</LightText>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent:'space-between', marginVertical:3 }}>
                                <DatePicker
                                style={{width: (width/2)-50 }}
                                date={this.state.startTime}
                                mode="time"
                                placeholder="00 AM"
                                format="hh:mm A"
                                minuteInterval={30}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                 onDateChange={(date) => {this.setState({startTime: date})}}
                            /> 

                            <DatePicker
                                style={{width: (width/2)-50}}
                                date={this.state.endTime}
                                mode="time"
                                placeholder="00 AM"
                                format="hh:mm A"
                                minuteInterval={30}
                                minDate={this.state.startTime}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                 onDateChange={(date) => {this.setState({endTime: date})}}
                            />                       
                                </View>
                                
                            </View>

                            )}
                                         <View style={{marginBottom: 10, height: 1, backgroundColor: 'rgba(139,139,139,.5)'}}/>
                                         
                            {this.props.user.userType === 'Rider' ? (
                                <View style={{ margin: 10}}>
                                <LightText> Ridng style: </LightText>
                                    <TextInput
                                            style={{ height: 45,fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                            placeholder ="Style"
                                            keyboardType={'default'}
                                            value={this.state.style}
                                            onChangeText={(style) => this.setState({style:style})}
                                        />
                                    </View>
                            ) : (
                                <View style={{ margin: 10}}>
                                <LightText> Link of RaceTrack: </LightText>
                                    <TextInput
                                            style={{ height: 45,fontSize:16, alignItems:"flex-start", color:'gray', marginLeft: 10}} 
                                            placeholder ="Link of RaceTrack"
                                            keyboardType={'default'}
                                            value={this.state.linkOfRaceTrack}
                                            onChangeText={(linkOfRaceTrack) => this.setState({linkOfRaceTrack:linkOfRaceTrack})}
                                        />
                                    </View>
                            )}
                            

                                <View style={{marginBottom: 10, height: 1, backgroundColor: 'rgba(139,139,139,.5)'}}/>
                                </View>

                                <TouchableOpacity
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    height: 45,
                                    borderRadius: 22.5,
                                    backgroundColor: "rgb(196,31,20)",
                                    marginHorizontal:30 ,
                                    marginTop: 40 
                                    }}
                                 onPress={() => this.validateUserDetails()}
                            >
                                <BoldText style={{color: 'white'}}>Save</BoldText>
                            </TouchableOpacity>
                            <View style={{height: 50}}/>
                    </View>
                    </ScrollView>
                </KeyboardAvoidingView>
             </TouchableWithoutFeedback>
             <Loader loading={this.props.loading} />
            </View>
        );
    }
}

const mapStateToProps = state =>{
    return{
        user: state.user.userData,
        loading: state.user.loading,
        uid: state.user.uid
    }
}
const mapDispatchToProps = dispatch =>{
    return{
        updateRiderProfile:(uid,name,email,age,about,hours,height,weight,image,userType,style, fromUserType, navigation) => updateRiderProfile(uid,name,email,age,about,hours,height,weight,image,userType,style, fromUserType, dispatch, navigation),
        updateTrainerProfile:(uid,name,email,about,image,startTime, endTime, linkOfRaceTrack,userType,fromUserType, navigation) => updateTrainerProfile(uid,name,email,about,image,startTime,endTime,linkOfRaceTrack,userType, fromUserType, dispatch, navigation)

    }
}
export default connect(mapStateToProps,mapDispatchToProps)(EditProfile);