import React, {Component} from 'react';
import{
View,
Image,
ScrollView,
Dimensions,
TouchableOpacity,
Modal,
TouchableWithoutFeedback,
Linking,
} from 'react-native';
import { connect } from 'react-redux';
import { logout } from '../store/actions/user';
import * as firebase from 'react-native-firebase';
import {calculateTimeDiff} from '../Utility/calculateTimeDiff';
import {
    BoldText, LightText
} from '../components/styledTexts';


const height = Dimensions.get('window').height;

class Profile extends Component{
        constructor(props){
            super(props)
                this.state = {
                    optionModelVisible: false
                }
        }

        optionModal() {
            return (
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.optionModelVisible}
                    onRequestClose={() => {
                        this.setState({optionModelVisible: false})
                    }}>
                    <TouchableWithoutFeedback onPress={() => this.setState({optionModelVisible: false})}>
                        <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.1)'}}>
                            <View style={{
                                backgroundColor: 'white', borderRadius: 5, width: 100, padding: 10, alignSelf: 'flex-end',
                                top: 65, right: '3%'
                            }}>
                                
                                    <TouchableOpacity style={{ flexDirection: 'row', alignItems:'center'}}
                                        onPress={() => {
                                            this.setState({optionModelVisible: false})
                                            this.props.navigation.navigate('EditProfile',{fromUserType:false})
                                        }} >
                                            <Image
                                                source = { require('../assets/edit-profile.png')}
                                            />
                                        <LightText style={{ marginHorizontal: 5, fontSize: 14, textAlign:'center'}}>Edit Profile</LightText>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={{ flexDirection: 'row',alignItems:'center', marginTop:5}}
                                     onPress={() => {
                                        firebase.auth().signOut();
                                        this.setState({optionModelVisible: false})
                                        this.props.logout(this.props.navigation);
                                        }} >
                                            <Image
                                                source = { require('../assets/log-out.png')}
                                            />
                                    <LightText style={{ marginHorizontal: 5,fontSize: 14, textAlign:'center'}}>Logout</LightText>
                                 </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            )
        }

        render(){
            return(
                <View style={{ flex: 1 }}>
                    {this.optionModal()}
 {/* Header  */}
                <View style={{ flex: .1, flexDirection:'row', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{width: 40}}/>
                    <View style={{flex: 1, alignItems:'center'}}>
                        <BoldText>Profile</BoldText>
                    </View>
                    <TouchableOpacity style={{ height: 40, width: 40, alignItems:'center', justifyContent:'center' }}
                                onPress = {() => this.setState({ optionModelVisible: true})}
                            >
                                <Image
                                    source = { require('./../assets/option.png')}
                                      style={{ height: 30, width: 30 }}
                                      resizeMode = {'contain'}
                                />
                            </TouchableOpacity>
                </View>

                <ScrollView style={{ flex: 1 }}>
                    <View style={{ flex:1 }}>
                        <Image
                        source = { this.props.user.image ? {uri: this.props.user.image} : require('./../assets/profiletemp.png')}
                        style = {{ flex: 1, height: height/3}}
                        resizeMode = 'cover'
                        />
                        <View style={{ bottom:5, paddingLeft: 10, width:'100%', position: 'absolute', justifyContent: 'flex-end', backgroundColor:'rgba(100,50,50,.1)' }}>
                            
                            {(this.props.user.userType === "Rider")?(
                                <BoldText style={{ color: 'white'}}>{this.props.user.name}, {this.props.user.age}</BoldText>
                            ):(
                                <BoldText style={{ color: 'white'}}>{this.props.user.name}</BoldText>

                            )}
                            <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                <Image
                                    source = { require('./../assets/mail.png')}
                                />
                                <BoldText style={{ color: 'white', fontSize: 12, paddingLeft: 5 }}>{this.props.user.email},  {this.props.user.userType}</BoldText>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, marginHorizontal: 10, marginVertical: 10 }}>
         {/* About Yourself */}
                        <View style={{ flexDirection:'row' }}>
                            <Image
                                source={ require('./../assets/about.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>About yourself:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.about}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
      {/* For Rider           */}
                {(this.props.user.userType === "Rider")?(
                    <View>
{/* Height */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/cap.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Height:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.height}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Weight */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/pills.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Weight:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.weight}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Riding Hours */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/clock.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Riding Hours:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.hours}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Riding Style */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/history.png')}
                                style={{ height: 20, width: 20, tintColor:'black' }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Riding Style:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.style}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>

                    </View>
                ):(
    //  For Trainer               
                    <View>
{/* Race Track */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/cap.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Race Track:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.racetrack}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Training Hours */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/clock.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Training Hours:</BoldText>
                        </View>
                        {/* <LightText style={{ paddingLeft: 30 }}>{calculateTimeDiff(this.props.user.startTime, this.props.user.endTime)}</LightText> */}
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.startTime + '-' + this.props.user.endTime}</LightText>

                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Number of training */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/training-hourse.png')}
                                style={{ height: 20, width: 20 }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Number of Training:</BoldText>
                        </View>
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.hours}</LightText>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>
        {/* Link of racetrack */}
                        <View style={{ flexDirection:'row', marginTop: 10 }}>
                            <Image
                                source={ require('./../assets/track-link.png')}
                                style={{ height: 20, width: 20, tintColor:'black' }}
                                resizeMode = 'contain'
                            />
                            <BoldText style={{ paddingLeft: 5, fontSize: 12, color: 'gray' }}>Link of RaceTrack:</BoldText>
                        </View>
                        <TouchableOpacity
                        onPress={() => {
                            const words = this.props.user.linkOfRaceTrack.split(':');
                            let racetrackLink = '';
                            if(words[0].toUpperCase() === 'HTTP' || words[0].toUpperCase() === 'HTTPS')
                            {
                                racetrackLink = this.props.user.linkOfRaceTrack
                            }else{
                                racetrackLink = "http://" + this.props.user.linkOfRaceTrack
                            }
                            Linking.canOpenURL(racetrackLink).then(supported => {
                                if (supported) {
                                  Linking.openURL(racetrackLink);
                                } else {
                                //   console.log("Don't know how to open URI: " + this.props.user.linkOfRaceTrack);
                                }
                              });              
                        }}
                        >
                        <LightText style={{ paddingLeft: 30 }}>{this.props.user.linkOfRaceTrack}</LightText>
                        </TouchableOpacity>
                        <View style={{ marginTop:10, height: 1, backgroundColor: 'rgba(245,245,245,1)' }}/>

                    </View>
                )
                }
                    </View>

                </ScrollView>
                </View>
            )
        }
};

const mapStateToProps = state =>{
    return{
        user:state.user.userData,
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        logout:(navigation) => logout(dispatch, navigation)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Profile);