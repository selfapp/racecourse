import React from 'react';
import { Dimensions, Platform, Image } from 'react-native';
import { createAppContainer, createBottomTabNavigator,createStackNavigator} from 'react-navigation';
import Splash from '../screens/Splash';
import Login from './../screens/Login';
import VerificationCheck from './../screens/VerificationCheck';
import IntroduceYourself from './../screens/IntroduceYourself';
import EditProfile from './../screens/EditProfile';
import History from './../screens/History';
import AppointmentList from '../screens/AppointmentList';
import Book from '../screens/Book';
import ViewProfile from '../screens/ViewProfile';
import Profile from './../screens/Profile';
import Availability from './../screens/Availability'
import AppointmentConfirm from './../screens/AppointmentConfirm';
import AppointmentSuccess from './../screens/AppointmentSuccess';

const iphoneHeight = Dimensions.get('window').height

export const loginNavigationOptions = createStackNavigator(
{
    Login:{
        screen: Login
    },
    VerificationCheck:{
        screen: VerificationCheck
    },
    IntroduceYourself:{
        screen: IntroduceYourself
    },
    EditProfile:{
        screen: EditProfile
    }
    },{
    headerMode: 'none'
    }
);

export const historyNavigationOptions = createStackNavigator(
    {
        History: {
            screen: History
        }
    },
    {
        headerMode: 'none'
    }
);

export const bookNavigationOptions = createStackNavigator(
    {
        Book: {
            screen: Book
        },
        ViewProfile:{
            screen:ViewProfile
        },
        Availability:{
            screen: Availability
        },
        AppointmentConfirm:{
            screen: AppointmentConfirm
        },
        AppointmentSuccess:{
            screen: AppointmentSuccess
        }
    },
    {
        headerMode: 'none'
    }
);

export const profileNavigationOptions = createStackNavigator(
    {
        Profile: {
            screen: Profile
        },
        EditProfile:{
            screen: EditProfile
        }
    },
    {
        headerMode: 'none'
    }
);

export const TabNavigator = createBottomTabNavigator(
    {
        HISTORY:{
            screen: historyNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./../assets/history.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        APPOINTMENT:{
            screen:AppointmentList,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./../assets/appointment.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        BOOK:{
            screen: bookNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./../assets/book.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        PROFILE:{
            screen: profileNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./../assets/profile.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        }
    },
    {
        initialRouteName: 'BOOK',        
        tabBarOptions: {
            activeTintColor: "rgb(196,31,20)",
            inactiveTintColor: "gray",
            showLabel: true,
            showIcon: true,
            tabBarPosition: 'bottom',
            labelStyle: {
                fontSize: 12,
            },
            iconStyle:{
                width: 30,
                height: 30
            },
            style: {
                backgroundColor: 'rgb(245,245,245)',
                borderBottomWidth: 1,
                borderBottomColor: '#ededed',
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
                // height: ((Platform.OS === 'ios' && iphoneHeight === 812) ? 20 : 55)
            },
            lazy: true,
            indicatorStyle: '#fff',
        }
    }
);

export const appNavigationOptions = createStackNavigator(
    {
        Splash:{
            screen: Splash
        },
        loginNavigationOptions:{
            screen: loginNavigationOptions
        },
        TabNavigator: {
            screen: TabNavigator
        }
    },
    {
        headerMode: 'none'
    }
);


export const AppContainer = createAppContainer(appNavigationOptions);