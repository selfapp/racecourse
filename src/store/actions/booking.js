import React from 'react';
import * as actionTypes from '../ActionTypes';
import firebase from 'react-native-firebase';
import { checkDateDiff, isBetween } from '../../Utility/calculateTimeDiff';

export const selectUserForBooking = (user, dispatch, navigation) => {
    dispatch({
        type: actionTypes.SELECT_USER_FOR_BOOKING,
        payload: user
    });
    navigation.navigate('Availability');
}

export const selectSlot = slot => {
    return {
        type: actionTypes.SELECT_SLOT,
        slot: slot
    }
};
export const selectDate = date =>{
    return {
        type: actionTypes.SELECT_DATE,
        date:date
    }
}

export const bookSlot = ( trainerID, riderID, date, slot, dispatch, navigation) =>{
    dispatch({
        type: actionTypes.BOOKING_START
    });
    firebase.database().ref('Booking/').push({
        date:date,
        trainerID: trainerID,
        riderID: riderID,
        slot: slot
    }).then(async()=>{
        dispatch({
            type: actionTypes.BOOKING_SUCCESS
        });
        navigation.navigate('AppointmentSuccess');
    })
}

export const fetchBooking = (date, userId, dispatch) => {
    dispatch({
        type: actionTypes.FETCH_BOOKINGLIST_START
    });

    firebase.database().ref('Booking/').orderByChild('date').equalTo(date).once('value', function (snapshot){
        let data = snapshot.val();
        if (data != null) {
            // console.log('booked list', data)
            let bookingList = [];
            snapshot.forEach(function (childSnapshot){
                let childData = childSnapshot.val();
                // console.log('child', childData)
                if(childData.trainerID === userId){
                    bookingList.push({
                        date: childData.date,
                        riderID: childData.riderID,
                        trainerID: childData.trainerID,
                        slot:childData.slot
                    })
                }
            });
            // console.log('List', bookingList)
            dispatch(fetchBookingSuccess(bookingList));
        }
      });
}
export const fetchBookingSuccess = bookingList => (
    {
    type: actionTypes.FETCH_BOOKING_SUCCESS,
    bookingList
  });

  export const fetchAppointmentList = (userId, userType,dispatch) =>{
    var appointmentList = [];

    dispatch({
        type: actionTypes.FETCH_APPOINTMENT_START
    });
    
    if(userType === 'Rider'){
        firebase.database().ref('Booking/').orderByChild('riderID').equalTo(userId).on('child_added', function (snapshot){
            let data = snapshot.val();
            if (data != null) {
                if(checkDateDiff(data.date) > 0){
                    firebase.database().ref('History/').push(snapshot.val())
                    var updates = {};
                        updates[snapshot.key] = null;
                        firebase.database().ref('Booking/').update(updates);
                }else if(checkDateDiff(data.date) === 0){
                
                   if(isBetween(data.slot.slot)){
                    firebase.database().ref('History/').push(snapshot.val())
                    var updates = {};
                        updates[snapshot.key] = null;
                        firebase.database().ref('Booking/').update(updates);
                   }else{
                    firebase.database().ref('Users/').orderByChild('user').equalTo(data.trainerID).on('child_added', function (user){
                        appointmentList.push({
                            user:user.val(),
                            data:data
                        });
                        dispatch(fetchAppointmentSuccess(appointmentList));
                    });
                   }
                }else{
                firebase.database().ref('Users/').orderByChild('user').equalTo(data.trainerID).on('child_added', function (user){
                    appointmentList.push({
                        user:user.val(),
                        data:data
                    });
                    dispatch(fetchAppointmentSuccess(appointmentList));
                });
            }
            }
          });
    }else{
        firebase.database().ref('Booking/').orderByChild('trainerID').equalTo(userId).on('child_added', function (snapshot){
            let data = snapshot.val();
            if (data != null) {       
                if(checkDateDiff(data.date) > 0){
                    firebase.database().ref('History/').push(snapshot.val())
                    var updates = {};
                        updates[snapshot.key] = null;
                        firebase.database().ref('Booking/').update(updates);
                    }else if(checkDateDiff(data.date) === 0){
                       if(isBetween(data.slot.slot)){
                        firebase.database().ref('History/').push(snapshot.val())
                        var updates = {};
                            updates[snapshot.key] = null;
                            firebase.database().ref('Booking/').update(updates);
                       }else{
                        firebase.database().ref('Users/').orderByChild('user').equalTo(data.trainerID).on('child_added', function (user){
                            appointmentList.push({
                                user:user.val(),
                                data:data
                            });
                            dispatch(fetchAppointmentSuccess(appointmentList));
                        });
                       }
                    }else{               
                    firebase.database().ref('Users/').orderByChild('user').equalTo(data.riderID).on('child_added', function (user){
                      appointmentList.push({
                        user:user.val(),
                        data:data
                    });
                    dispatch(fetchAppointmentSuccess(appointmentList));
                });
            }
            }
          });
    }
  }
  
  export const fetchAppointmentSuccess = appointmentList => (
    {
    type: actionTypes.FETCH_APPOINTMENT_SUCCESS,
    appointmentList
  });

  //History

  export const fetchHistoryList = (userId, userType,dispatch) =>{
    var historyList = [];

    dispatch({
        type: actionTypes.FETCH_HISTORY_START
    });
    
    if(userType === 'Rider'){
        firebase.database().ref('History/').orderByChild('riderID').equalTo(userId).on('child_added', function (snapshot){
            let data = snapshot.val();
            if (data != null) {
                firebase.database().ref('Users/').orderByChild('user').equalTo(data.trainerID).on('child_added', function (user){
                    historyList.push({
                        user:user.val(),
                        data:data
                    });
                    dispatch(fetchHistorySuccess(historyList));
                });
            }
          });
    }else{
        firebase.database().ref('History/').orderByChild('trainerID').equalTo(userId).on('child_added', function (snapshot){
            let data = snapshot.val();
            if (data != null) {            
                firebase.database().ref('Users/').orderByChild('user').equalTo(data.riderID).on('child_added', function (user){
                    historyList.push({
                        user:user.val(),
                        data:data
                    });
                    dispatch(fetchHistorySuccess(historyList));
                });
            }
          });
    }
  }
  
  export const fetchHistorySuccess = historyList => (
    {
    type: actionTypes.FETCH_HISTORY_SUCCESS,
    historyList
  });

