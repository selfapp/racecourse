import{
    TYPE_MOBILE,
    CHOOSE_COUNTRY,
    LOGIN_START,
    LOGIN_ERROR,
    LOGIN_OTP,
    OTP_SUCCESS,
    LOGIN_SUCCESS,
    UPDATE_USER_UID,
    UPDATE_USER_DATA,
    APPOINTMENT_ERROR,
    USER_LOGOUT,
    RESEND_OTP,
    RESEND_SUCCESS
} from '../ActionTypes';
import * as firebase from 'react-native-firebase';
import { StackActions, NavigationActions } from 'react-navigation';


export const typeMobile = phone_number => {
    return {
        type: TYPE_MOBILE,
        payload: phone_number
    }
};

export const chooseCountry = country_code => {
    return {
        type: CHOOSE_COUNTRY,
        payload: country_code
    }
};

export const login = (country_code, phone_number, dispatch, navigate) =>{
    if(phone_number.length === 10){
        dispatch({
            type:LOGIN_START
        });
        // console.log(`+${country_code}${phone_number}`);
        firebase.auth().signInWithPhoneNumber(`+${country_code}${phone_number}`)
        .then(confirmResult => {
            dispatch({
                type: LOGIN_OTP,
                payload: confirmResult
            });
            // console.log(JSON.stringify(confirmResult));
            navigate('VerificationCheck',{
                phoneNumber: phone_number,
                country_code: country_code
            });
            
        })
        .catch(error => {
            
            dispatch({
                type: LOGIN_ERROR,
                payload:{
                    phone_number:[ `Sign In With Phone Number Error: ${error.message}`]
                }
            });
        });
        
    }else{
        dispatch({
            type: LOGIN_ERROR,
            payload:{
                phone_number:['Kindly enter 10 digit phone number']
            }
        });
    }
};

export const reSendOtp = (country_code, phone_number, dispatch) =>{

    dispatch({
        type: RESEND_OTP,
    });
    firebase.auth().signInWithPhoneNumber(`+${country_code}${phone_number}`, true)
    .then(confirmResult => {
        dispatch({
            type: RESEND_SUCCESS,
        });
        alert('OTP resend successfully.')
        // console.log(JSON.stringify(confirmResult));
    })
}

export const verifyOTP = (code,confirmResult, dispatch, navigation) => {
    if (confirmResult && code.length === 6) {
        dispatch({
            type: LOGIN_START
        });
        confirmResult.confirm(code)
        .then((user) => {
                // console.log('user Verified')
                dispatch({
                    type: OTP_SUCCESS,
                    payload:user.uid
                });
        })
        .catch(error =>{
            dispatch({
                type: LOGIN_ERROR,
                payload: {
                    otp: [`Code Confirm Error: ${error.message}`]
                }
            });
        }
        );
    } else {
        dispatch({
            type: LOGIN_ERROR,
            payload: {
                otp: ['Please enter the correct OTP']
            }
        });
    }
};

export const setUserType = (userType, uid, dispatch, navigation) => {
    dispatch({
        type: LOGIN_START
    });

    firebase.database().ref('Users/' + uid).set({
        isProfileCompleted: false,
        userType:userType,
    }).then(()=>{
        dispatch({
            type: LOGIN_SUCCESS
        });
        navigation.navigate('EditProfile',{fromUserType:true});
    })
}

export const updateRiderProfile = (uid,name, email,age,about,hours,height,weight,image,userType,style, fromUserType, dispatch, navigation) => {
    dispatch({
        type: LOGIN_START
    });
    firebase.database().ref('Users/' + uid).set({
        user:uid,
        isProfileCompleted: true,
        name:name,
        email:email,
        image:image,
        about:about,
        height:height,
        weight:weight,
        userType:userType,
        age:age,
        hours:hours,
        style:style
    }).then(async()=>{
        dispatch({
            type: LOGIN_SUCCESS
        });
        if(fromUserType){
        navigation.navigate('TabNavigator');
        }else{
            navigation.goBack()
        }
    })
}
export const updateTrainerProfile = (uid,name, email,about,image,startTime,endTime,linkOfRaceTrack ,userType, fromUserType, dispatch, navigation) => {
    dispatch({
        type: LOGIN_START
    });
    firebase.database().ref('Users/' + uid).set({
        user:uid,
        isProfileCompleted: true,
        name:name,
        email:email,
        image:image,
        about:about,
        userType:userType,
        startTime:startTime,
        endTime:endTime,
        linkOfRaceTrack:linkOfRaceTrack
    }).then(async()=>{
        dispatch({
            type: LOGIN_SUCCESS
        });
        if(fromUserType){
        navigation.navigate('TabNavigator');
        }else{
            navigation.goBack()
        }
    })
}

export const updateUid = (uid, dispatch) => {
    dispatch({
        type: UPDATE_USER_UID,
        uid 
    });
}

export const updateUserData = (userData, dispatch) =>{
    dispatch({
        type:UPDATE_USER_DATA,
        userData
    })
}


export const navigateToConfirm = (selectedSlot , dispatch, navigation) =>{
    if(selectedSlot != null){
        navigation.navigate('AppointmentConfirm');
    }else{
        dispatch({
            type: APPOINTMENT_ERROR,
            payload:{
                select_slot:['Kindly select a time slot']
            }
        });
        
    } 
};

export const navigateToSuccess = (navigation) =>{
    navigation.navigate('AppointmentSuccess');
};

export const logout =(dispatch, navigation) =>{

    firebase.auth().signOut();
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'loginNavigationOptions' })],
      });
      dispatch({
        type: USER_LOGOUT
    });
    navigation.dispatch(resetAction);
}