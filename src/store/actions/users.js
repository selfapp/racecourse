import * as actionTypes from '../ActionTypes';
import firebase from 'react-native-firebase';


export const fetchUsers = (userType,dispatch) => {
    var usersData=[];
    dispatch({
        type: actionTypes.FETCH_USERS_START
      });
      firebase.database().ref('Users/').orderByChild('userType').equalTo(userType).on('child_added', function (snapshot){
        let data = snapshot.val();
        if (data != null) {
            usersData.push(data);
            dispatch(fetchUsersSuccess(usersData));
        }
      });
}

export const fetchUsersSuccess = users =>(
    {
    type: actionTypes.FETCH_USERS_SUCCESS,
    users
  });