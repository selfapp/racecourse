import * as actionTypes from '../ActionTypes';


const initialState ={
    userList:[],
    loading:false,
    errors: {}
}

export const usersReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.FETCH_USERS_START:{
            return{
                ...state,
                loading:true
            }
        }
        case actionTypes.FETCH_USERS_SUCCESS:{
            return{
                ...state,
                loading:false,
                userList: action.users
            }
        }
        default:
            return state;

    }
}
export default usersReducer;