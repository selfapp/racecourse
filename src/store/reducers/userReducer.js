import * as actionTypes from '../ActionTypes';

const initialState ={
    phone_number: '',
    country_code:"1",
    loading:false,
    confirmResult: null,
    userData: null,
    uid: null,
    errors: {},
    message:null
}

export const userReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.TYPE_MOBILE: {
            return {
                ...state,
                phone_number: action.payload,
                errors: {}
            }
        }
        case actionTypes.CHOOSE_COUNTRY: {
            return {
                ...state,
                country_code: action.payload
            }
        }
        case actionTypes.LOGIN_START: {
            return {
                ...state,
                loading: true,
                errors: {}
            }
        }
        case actionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                loading: false,
                errors: {}
            }
        }
        case actionTypes.LOGIN_ERROR: {
            return {
                ...state,
                loading: false,
                errors: action.payload
            }
        }
        case actionTypes.RESEND_OTP: {
            return {
                ...state,
                loading: true,
                errors: {}
            }
        }
        case actionTypes.RESEND_SUCCESS: {
            return {
                ...state,
                loading: false,
                message: 'OTP resend successfully.'
            }
        }
        case actionTypes.LOGIN_OTP: {
            return {
                ...state,
                confirmResult:action.payload,
                loading: false,
                errors: {}
            }
        }

        case actionTypes.LOGIN_FAILED: {
            return {
                ...state,
                loading: false,
                errors: {
                    message: action.payload
                }
            }
        }
        case actionTypes.OTP_SUCCESS: {
            return {
                ...state,
                loading: false,
                uid: action.payload
            }
        }
        
        case actionTypes.UPDATE_USER_UID:
            return {
                ...state,
                uid: action.uid
            }
        case actionTypes.UPDATE_USER_DATA:
            return{
                ...state,
                userData:action.userData
            }
        case actionTypes.APPOINTMENT_ERROR:{
            return {
                ...state,
                errors: action.payload
            }
        }
        case actionTypes.USER_LOGOUT:{
            return {
                ...state,
                phone_number: '',
                country_code:"1",
                loading:false,
                confirmResult: null,
                userData: null,
                uid: null,
                errors: {}
            }
        }

        default:
            return state
    }
}