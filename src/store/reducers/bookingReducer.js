import * as actionTypes from '../ActionTypes';
import moment from "moment";

const initialState ={
    selectedSlot:null,
    user:null,
    selectedDate:new Date(Date.now()),
    errors: {},
    bookingList:[],
    appointmentList:[],
    historyList:[],
    loading:false

}

export const bookingReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SELECT_USER_FOR_BOOKING:{
            return {
                ...state,
                user: action.payload
            }
        }

        case actionTypes.SELECT_SLOT:{
            // let slotCopy = state.selectedSlots;
            // if(action.slot.selected){
            //         slotCopy = [...state.selectedSlots, action.slot]
            // }else{
            //     slotCopy.map((item,index)=>{
            //         if(item.id === action.slot.id){
            //             slotCopy.splice(index,1)
            //         }
            //     })
            // }
            return {
                ...state,
                selectedSlot:action.slot
                // selectedSlots: slotCopy
            }
        }
        case actionTypes.SELECT_DATE:{
            return {
                ...state,
                selectedDate: action.date
            }
        }
        case actionTypes.BOOKING_START:{
            return{
                ...state,
                loading: true
            }
        }
        case actionTypes.BOOKING_SUCCESS:{
            return{
                ...state,
                loading: false,
                // selectedSlot: null,
                // user: null,
                // selectedDate: new Date(Date.now())
            }
        }
        case actionTypes.FETCH_BOOKING_SUCCESS:{
            return {
                ...state,
                bookingList: action.bookingList
            }
        }
        case actionTypes.FETCH_BOOKINGLIST_START:{
            return {
                ...state,
                selectedSlot: null,
                bookingList:[]
            }
        }
        case actionTypes.FETCH_APPOINTMENT_START:{
            return {
                ...state,
                appointmentList:[]
                // loading: true
            }
        }
        case actionTypes.FETCH_APPOINTMENT_SUCCESS:{
            // let appointmentCopy = []
            //     console.log('apointment', appointmentCopy)
            //     appointmentCopy = action.appointmentList.sort((a,b) => new moment(a.data.date) - new moment(b.data.date))
            //     console.log('sorted data', appointmentCopy)
            return {
                ...state,
                appointmentList: action.appointmentList,
                // loading: false
            }
        }
        
        case actionTypes.FETCH_HISTORY_START:{
            return {
                ...state,
                historyList:[]
                // loading: true
            }
        }
        case actionTypes.FETCH_HISTORY_SUCCESS:{
            return {
                ...state,
                historyList: action.historyList,
                // loading: false
            }
        }
        default:
            return {...state}
    }
}