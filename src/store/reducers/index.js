import { combineReducers } from 'redux';
import { userReducer } from './userReducer';
import { usersReducer } from './usersReducer';
import { bookingReducer } from './bookingReducer';

export default combineReducers({
    user: userReducer,
    users:usersReducer,
    booking:bookingReducer
});