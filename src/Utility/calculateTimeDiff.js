import moment from 'moment';

export const calculateTimeDiff = (start, end) =>{
    let duration = moment.duration(moment(end, 'hh:mm A').diff(moment(start, 'hh:mm A'),'hours'));
    return duration.asMilliseconds();
}

export const checkDateDiff = (date) =>{
    
    var strSplitDate = date.split(' ');
    let data = moment.duration(moment().diff(moment(stringToDate(strSplitDate[1]))));
    var days = parseInt(data.asDays());
    return (days)
}
export const stringToDate = function(dateString) {
    const [dd, mm, yyyy] = dateString.split("-");
    return new Date(`${yyyy}-${mm}-${dd}`);
  };

  export const isBetween = (slot)=>{
    var currentTime= moment();
    var strSplitDate = slot.split('-');
    var startTime = moment(strSplitDate[1], "HH:mm a");
    var endTime = moment('11:59 pm', "HH:mm a");
   var amIBetween = currentTime.isBetween(startTime , endTime);
    return amIBetween
  }