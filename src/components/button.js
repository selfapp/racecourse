
import React, {Component}  from "react";
import { 
  TouchableOpacity,
  Image,
  View
 } from "react-native";
import {BoldText, LightText} from '../components/styledTexts';


export class OptionButton extends Component {
    render(){
      // console.log('value', this.props.disabled)
      return (
        <View style={this.props.style}>
        <TouchableOpacity style={{ height: 50, flexDirection: 'row', alignItems:'center'}}
                 onPress={()=> this.props.onPress()}
                 disabled = {this.props.disabled}

              >
              {this.props.disabled ? (
                <View style={{flexDirection: 'row', alignItems:'center' }}>                  
                  <Image
                  style={{ height: 30, width: 30, tintColor: 'green'}}
                  source={require('../assets/check.png')}
                />
                  <LightText style={{ color: 'green', marginLeft: 10 }}>{this.props.value}</LightText>
                  </View>

                ) : (
                  <View style={{flexDirection: 'row', alignItems:'center' }}>                  
                    <Image
                    style={{ height: 30, width: 30, tintColor: (this.props.active) ? this.props.color: 'gray'}}
                    source={require('../assets/check.png')}
                  />
                  <LightText style={{ color: (this.props.active) ? this.props.color : 'gray', marginLeft: 10 }}>{this.props.value}</LightText>
                  </View>
                )}
                
              </TouchableOpacity>
        </View>
    )
    }
  };